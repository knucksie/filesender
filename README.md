# README #

### What is this repository for? ###

* This is a simple file sender web application based on cloud functions. For better serstanding search for "Firefox Send".
* The project is written to be nteroperable with Yandex Cloud Functions and AWS S3 compatible storage.
* [Yandex Cloud Functions docs](https://cloud.yandex.ru/docs/functions/)

### How do I get set up? ###

1. Make sure you have an active Yandex Cloud account.
2. Make sure you have an active AWS S3 compatible storage.
3. Deploy each function. Make sure requirements file is located in each. 
4. Link fucntions via url in code editor.
5. Set "AWS_ACCESS_KEY_ID" env variable. Specify your aws access key id.
6. Set "AWS_SECRET_ACCESS_KEY" env variable. Specify your secret key.

### To do: ###

* Add files encryption.
* Add functions linking through environmental variables. 

### Who do I talk to? ###

* @knucksie 