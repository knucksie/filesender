from jinja2 import Environment, FileSystemLoader, select_autoescape

DOWNLOAD_FUNCTION_URL = 'https://functions.yandexcloud.net/d4ebgo7g5pan5qnoi4d4'

template = None
TEMPLATE_FILE_NAME = 'link_template_v2.html'


# creating Jinja template object
def get_template():
    global template
    if not template:
        env = Environment(
            loader=FileSystemLoader('.'),
            autoescape=select_autoescape(['html'])
        )
        template = env.get_template(TEMPLATE_FILE_NAME)
    return template


# rendering page with a link to uploaded object
def handler(event, context):
    key = event['queryStringParameters']['k']
    template = get_template()
    html = template.render(
        download_link=f'{DOWNLOAD_FUNCTION_URL}?k={key}'
    )
    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'text/html'
        },
        'body': html,
    }
