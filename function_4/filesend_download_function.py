import boto3
import botocore
from os import getenv
import io

endpoint = 'https://storage.yandexcloud.net'
AWS_ACCESS_KEY_ID = getenv('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = getenv('AWS_SECRET_ACCESS_KEY')
SUCCESSFUL_UPLOAD_FUNCTION_URL = 'https://functions.yandexcloud.net/d4e9ibmc855oa1m3kaa8'
s3_client = None
BUCKET_NAME = 'filesend-bucket'


def get_client():
    global s3_client
    if not s3_client:
        s3_client = boto3.client('s3',
                                 aws_access_key_id=AWS_ACCESS_KEY_ID,
                                 aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                                 region_name='ru-central1',
                                 endpoint_url=endpoint,
                                 config=botocore.client.Config(signature_version='s3v4'),
                                 )
    return s3_client


# downloading and deleting specified file
def get_and_delete(key):
    s3_client = get_client()
    file = io.BytesIO()
    s3_client.download_fileobj(BUCKET_NAME, key, file)
    s3_client.delete_object(Bucket=BUCKET_NAME, Key=key)
    return file


def handler(event, context):
    key = event['queryStringParameters']['k']
    file = get_and_delete(key)
    # retrieving filename from the key
    key, filename = key.split('/')
    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'multipart/form-data',
            'Content-Disposition': f'attachment; filename="{filename}"'
        },
        'body': file.read()
    }
