import boto3
import botocore
from os import getenv
from jinja2 import Environment, select_autoescape, FileSystemLoader
import uuid

endpoint = 'https://storage.yandexcloud.net'
AWS_ACCESS_KEY_ID = getenv('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = getenv('AWS_SECRET_ACCESS_KEY')
# TODO: link functions through env variables
SUCCESSFUL_UPLOAD_FUNCTION_URL = 'https://functions.yandexcloud.net/d4e9ibmc855oa1m3kaa8'
s3_client = None
TEMPLATE_FILE_NAME = 'template_v2.html'

# s3 client initialization
def get_client():
    global s3_client
    if not s3_client:
        s3_client = boto3.client('s3',
                                 aws_access_key_id=AWS_ACCESS_KEY_ID,
                                 aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                                 region_name='ru-central1',
                                 endpoint_url=endpoint,
                                 config=botocore.client.Config(signature_version='s3v4'),
                                 )
    return s3_client


# getting form fields as described in  https://cloud.yandex.ru/docs/storage/concepts/presigned-post-forms
def get_form_fields():
    s3_client = get_client()
    # file unique identifier
    key_prefix = str(uuid.uuid4())
    key = key_prefix + '/${filename}'
    bucket = 'filesend-bucket'
    # security policy is described below
    conditions = [{"acl": "public-read"},
                  ["starts-with", "$key", key_prefix],
                  ["starts-with", "$success_action_redirect", SUCCESSFUL_UPLOAD_FUNCTION_URL]]
    # redirect uri is a next function url with key as a query string parameter
    fields = {'success_action_redirect': f'{SUCCESSFUL_UPLOAD_FUNCTION_URL}?k={key_prefix}'}

    prepared_form_fields = s3_client.generate_presigned_post(Bucket=bucket,
                                                             Key=key,
                                                             Conditions=conditions,
                                                             Fields=fields,
                                                             ExpiresIn=60 * 60)
    return prepared_form_fields


template = None


# creating Jinja template object
def get_template():
    global template
    if not template:
        env = Environment(
            loader=FileSystemLoader('.'),
            autoescape=select_autoescape(['html'])
        )
        template = env.get_template(TEMPLATE_FILE_NAME)
    return template


# getting rendered form
def get_form_page():
    fields = get_form_fields()['fields']
    template = get_template()
    rendered_page = template.render(
        key=fields['key'],
        x_amz_credential=fields['x-amz-credential'],
        x_amz_algorithm=fields['x-amz-algorithm'],
        x_amz_date=fields['x-amz-date'],
        policy=fields['policy'],
        x_amz_signature=fields['x-amz-signature'],
        success_action_redirect=fields['success_action_redirect']
    )
    return rendered_page


# sending form to user
def handler(event, context):
    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'text/html',
        },
        'body': get_form_page(),
    }
