import boto3
import botocore
from os import getenv
from jinja2 import FileSystemLoader, Environment, select_autoescape

endpoint = 'https://storage.yandexcloud.net'
AWS_ACCESS_KEY_ID = getenv('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = getenv('AWS_SECRET_ACCESS_KEY')
s3_client = None
BUCKET_NAME = 'filesend-bucket'
# next function url
DOWNLOADER_URL = 'https://functions.yandexcloud.net/d4e930jctpu96hn87ngl'

TEMPLATE_FILE_NAME = 'file_template_v2.html'


# TODO: write comments using docstrings

def get_client():
    global s3_client
    if not s3_client:
        s3_client = boto3.client('s3',
                                 aws_access_key_id=AWS_ACCESS_KEY_ID,
                                 aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                                 region_name='ru-central1',
                                 endpoint_url=endpoint,
                                 config=botocore.client.Config(signature_version='s3v4'),
                                 )
    return s3_client


template = None


# creating Jinja template object
def get_template():
    global template
    if not template:
        env = Environment(
            loader=FileSystemLoader('.'),
            autoescape=select_autoescape(['html'])
        )
        template = env.get_template(TEMPLATE_FILE_NAME)
    return template


# invalid link exception
class IncorrectLinkError(Exception):
    pass


# get filename by file key (identifier)
def get_filename(key):
    s3_client = get_client()
    objects_response = s3_client.list_objects(Bucket=BUCKET_NAME)
    if 'Contents' in objects_response:
        objects = objects_response['Contents']
        for object in objects:
            if object['Key'].startswith(key):
                return object['Key'].replace(f'{key}/', '')
    raise IncorrectLinkError


# return a page with a file name and 'download' button
def handler(event, context):
    template = get_template()
    try:
        key = event['queryStringParameters']['k']
        filename = get_filename(key)
        rendered_page = template.render(
            filename=filename,
            download_link=f'{DOWNLOADER_URL}?{key}/{filename}'
        )
        return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'text/html'
            },
            'body': rendered_page
        }
    # if link is invalid
    except IncorrectLinkError:
        return {
            'statusCode': 200,
            'headers': {
                'Content-Type': 'text/plain'
            },
            'body': 'Link is not valid anymore.'
        }
